# prova-desenvolvimento

Projeto criado como requisito de um processo seletivo.

Tecnologias utilizadas:
 -> Python: 3.7.4
 -> Django: 3.0.3
 -> Django Widget Tweaks: 1.4.5
 -> Django Bootstrap Modal Forms: 1.5.0
 -> MySQL: 8.0.19
 -> HTML5, CSS3, JavaScript e Jquery.