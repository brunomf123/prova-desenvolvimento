/* Definindo as chamativas de modais para os botões de 'criar', 'alterar'
e 'deletar' na página referente ao 'Tipo de Processo' */
$(function () {
    $(".tipo-processo-create").modalForm({
        formURL: $(".tipo-processo-create").data('url')
    });
    $(".tipo-processo-update").each(function () {
        $(this).modalForm({
            formURL: $(this).data('url')
        });
    });
    $(".tipo-processo-delete").each(function () {
        $(this).modalForm({
            formURL: $(this).data('url')
        });
    })
});

// Criação de uma função simples para a limpeza e pesquisa do filtro.
function clean_and_search() {
    $("#filter_search").val("")
    $("#filter_search_button").click()
}