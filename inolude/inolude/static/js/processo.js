/* Definindo as chamativas de modais para os botões de 'criar', 'alterar'
e 'deletar' na página referente ao 'Processo' */
$(function () {
    $(".processo-create").modalForm({
        formURL: $(".processo-create").data('url')
    });
    $(".processo-update").each(function () {
        $(this).modalForm({
            formURL: $(this).data('url')
        });
    });
    $(".processo-delete").each(function () {
        $(this).modalForm({
            formURL: $(this).data('url')
        });
    })
    $(".processo-delete").each(function () {
        $(this).modalForm({
            formURL: $(this).data('url')
        });
    })
});

// Criação de uma função para a validação do formato e do tamanho do arquivo.
function ValidateFile(file) {
    // Variável regex para verificar as extensões permitidas.
    let allowedExtensions = /(\.pdf)$/i;
    let filePath = file.value;

    // Caso o arquivo inserido não seja permitido, levanta um alerta e limpa o campo.
    if (!allowedExtensions.exec(filePath)) {
        alert('Tipo de arquivo inválido! Por favor, insira um arquivo PDF!');
        $(file).val('');
    } else {
        // Caso contrário, o tamanho é validado.
        // Conversão do tamanho do arquivo para MB.
        let FileSize = file.files[0].size / 1024 / 1024;

        // Caso seja maior que 10MB, levanta um alerta e limpa o campo.
        if (FileSize > 10) {
            alert('O arquivo excede os 10mb limite! Por favor, insira outro arquivo!');
            $(file).val('');
        } else {
            // Caso contrário, é um arquivo válido e adicionamos seu nome na label do input.
            $('.custom-file-label').html(file.files[0].name);
        }
    }
}

// Criação de uma função simples para a limpeza e pesquisa do filtro.
function clean_and_search() {
    $("#filter_search").val("")
    $("#filter_search_button").click()
}