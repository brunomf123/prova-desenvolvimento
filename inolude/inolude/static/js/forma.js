/* Definindo as chamativas de modais para os botões de 'criar', 'alterar'
e 'deletar' na página referente à 'Forma' */
$(function () {
    $(".forma-create").modalForm({
        formURL: $(".forma-create").data('url')
    });
    $(".forma-update").each(function () {
        $(this).modalForm({
            formURL: $(this).data('url')
        });
    });
    $(".forma-delete").each(function () {
        $(this).modalForm({
            formURL: $(this).data('url')
        });
    })
});

// Criação de uma função simples para a limpeza e pesquisa do filtro.
function clean_and_search() {
    $("#filter_search").val("")
    $("#filter_search_button").click()
}