/* Definindo as chamativas de modais para os botões de 'criar', 'alterar'
e 'deletar' na página referente à 'Matéria' */
$(function () {
    $(".materia-create").modalForm({
        formURL: $(".materia-create").data('url')
    });
    $(".materia-update").each(function () {
        $(this).modalForm({
            formURL: $(this).data('url')
        });
    });
    $(".materia-delete").each(function () {
        $(this).modalForm({
            formURL: $(this).data('url')
        });
    })
});

// Criação de uma função simples para a limpeza e pesquisa do filtro.
function clean_and_search() {
    $("#filter_search").val("")
    $("#filter_search_button").click()
}