from django.urls import path
from jurisdicao import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),

    path('formas', views.FormaList.as_view(), name='list_forma'),
    path('formas/create', views.FormaCreate.as_view(), name='create_forma'),
    path('formas/update/<int:pk>', views.FormaUpdate.as_view(), name='update_forma'),
    path('formas/delete/<int:pk>', views.FormaDelete.as_view(), name='delete_forma'),

    path('materias', views.MateriaList.as_view(), name='list_materia'),
    path('materias/create', views.MateriaCreate.as_view(), name='create_materia'),
    path('materias/update/<int:pk>', views.MateriaUpdate.as_view(), name='update_materia'),
    path('materias/delete/<int:pk>', views.MateriaDelete.as_view(), name='delete_materia'),

    path('tipos_processo', views.TipoProcessoList.as_view(), name='list_tipo_processo'),
    path('tipos_processo/create', views.TipoProcessoCreate.as_view(), name='create_tipo_processo'),
    path('tipos_processo/update/<int:pk>', views.TipoProcessoUpdate.as_view(), name='update_tipo_processo'),
    path('tipos_processo/delete/<int:pk>', views.TipoProcessoDelete.as_view(), name='delete_tipo_processo'),

    path('processos', views.ProcessoList.as_view(), name='list_processo'),
    path('processos/create', views.ProcessoCreate.as_view(), name='create_processo'),
    path('processos/download/<int:pk>', views.download_blob, name='download_processo'),
    path('processos/update/<int:pk>', views.ProcessoUpdate.as_view(), name='update_processo'),
    path('processos/delete/<int:pk>', views.ProcessoDelete.as_view(), name='delete_processo'),
]
