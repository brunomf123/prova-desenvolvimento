# Generated by Django 3.0.3 on 2020-02-10 20:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jurisdicao', '0005_auto_20200209_1234'),
    ]

    operations = [
        migrations.AlterField(
            model_name='processo',
            name='vl_processo',
            field=models.DecimalField(blank=True, db_column='vl_Processo', decimal_places=2, max_digits=21, null=True),
        ),
    ]
