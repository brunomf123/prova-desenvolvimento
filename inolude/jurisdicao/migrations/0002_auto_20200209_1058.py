# Generated by Django 3.0.3 on 2020-02-09 13:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jurisdicao', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='processo',
            name='ar_processo',
            field=models.FileField(db_column='ar_Processo', upload_to=''),
        ),
        migrations.AlterField(
            model_name='processo',
            name='dt_processo',
            field=models.DateField(db_column='dt_Processo'),
        ),
    ]
