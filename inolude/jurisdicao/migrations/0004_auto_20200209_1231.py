# Generated by Django 3.0.3 on 2020-02-09 15:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jurisdicao', '0003_auto_20200209_1114'),
    ]

    operations = [
        migrations.AlterField(
            model_name='processo',
            name='ar_processo',
            field=models.FileField(db_column='ar_Processo', upload_to=''),
        ),
    ]
