from django.apps import AppConfig


class JurisdicaoConfig(AppConfig):
    name = 'jurisdicao'
