import base64

from django.contrib import messages
from django.db.models import ProtectedError
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import ListView, TemplateView
from bootstrap_modal_forms.generic import BSModalCreateView, BSModalUpdateView, BSModalDeleteView

from jurisdicao.forms import FormaForm, MateriaForm, TipoProcessoForm, ProcessoForm
from jurisdicao.models import Forma, Materia, Processo, TipoProcesso

"""
@Author: Bruno Morete Felix
@Date: 08/02/2020

Criação da View referente ao Início do sistema.
"""


class IndexView(TemplateView):
    template_name = 'index.html'


"""
@Author: Bruno Morete Felix
@Date: 08/02/2020

Criação das Views referentes ao CRUD da classe 'Forma'.
"""


class FormaList(ListView):
    paginate_by = 5
    template_name = 'forma/list_forma.html'
    model = Forma
    ordering = ['de_forma']

    # Bruno - 10/01/2020: reescrita dos métodos 'get_queryset' e 'get_context_data' para realizar filtragem por GET
    def get_queryset(self):
        filter_val = self.request.GET.get('filter', '')
        new_context = self.model.objects.filter(de_forma__icontains=filter_val)
        return new_context

    def get_context_data(self, **kwargs):
        context = super(FormaList, self).get_context_data(**kwargs)
        context['filter'] = self.request.GET.get('filter', '')
        return context


class FormaCreate(BSModalCreateView):
    form_class = FormaForm
    template_name = 'forma/create_forma.html'
    model = Forma
    success_url = reverse_lazy('list_forma')
    success_message = 'Forma cadastrada com sucesso!'


class FormaUpdate(BSModalUpdateView):
    form_class = FormaForm
    template_name = 'forma/update_forma.html'
    model = Forma
    success_url = reverse_lazy('list_forma')
    success_message = 'Forma editada com sucesso!'


class FormaDelete(BSModalDeleteView):
    template_name = 'forma/confirm_delete_forma.html'
    model = Forma
    success_url = reverse_lazy('list_forma')
    success_message = 'Forma deletada com sucesso!'

    # Bruno - 10/01/2020: reescrita do método 'post' para realizar a validação de erro de integridade relacional.
    def post(self, request, *args, **kwargs):
        try:
            self.get_object().delete()
            messages.add_message(request, messages.SUCCESS, self.success_message)
            return redirect(self.success_url)
        except ProtectedError:
            messages.add_message(request, messages.ERROR, 'Erro: registro já está sendo utilizado!')
            return redirect(reverse_lazy('list_forma'))


"""
@Author: Bruno Morete Felix
@Date: 08/02/2020

Criação das Views referentes ao CRUD da classe 'Materia'.
"""


class MateriaList(ListView):
    paginate_by = 5
    template_name = 'materia/list_materia.html'
    model = Materia
    ordering = ['de_tipomateria']

    # Bruno - 10/01/2020: reescrita dos métodos 'get_queryset' e 'get_context_data' para realizar filtragem por GET
    def get_queryset(self):
        filter_val = self.request.GET.get('filter', '')
        new_context = self.model.objects.filter(de_tipomateria__icontains=filter_val)
        return new_context

    def get_context_data(self, **kwargs):
        context = super(MateriaList, self).get_context_data(**kwargs)
        context['filter'] = self.request.GET.get('filter', '')
        return context


class MateriaCreate(BSModalCreateView):
    form_class = MateriaForm
    template_name = 'materia/create_materia.html'
    model = Materia
    success_url = reverse_lazy('list_materia')
    success_message = 'Matéria cadastrada com sucesso!'


class MateriaUpdate(BSModalUpdateView):
    form_class = MateriaForm
    template_name = 'materia/update_materia.html'
    model = Materia
    success_url = reverse_lazy('list_materia')
    success_message = 'Matéria editada com sucesso!'


class MateriaDelete(BSModalDeleteView):
    template_name = 'materia/confirm_delete_materia.html'
    model = Materia
    success_url = reverse_lazy('list_materia')
    success_message = 'Matéria deletada com sucesso!'

    # Bruno - 10/01/2020: reescrita do método 'post' para realizar a validação de erro de integridade relacional.
    def post(self, request, *args, **kwargs):
        try:
            self.get_object().delete()
            messages.add_message(request, messages.SUCCESS, self.success_message)
            return redirect(self.success_url)
        except ProtectedError:
            messages.add_message(request, messages.ERROR, 'Erro: registro já está sendo utilizado!')
            return redirect(reverse_lazy('list_materia'))


"""
@Author: Bruno Morete Felix
@Date: 08/02/2020

Criação das Views referentes ao CRUD da classe 'TipoProcesso'.
"""


class TipoProcessoList(ListView):
    paginate_by = 5
    template_name = 'tipo_processo/list_tipo_processo.html'
    model = TipoProcesso
    ordering = ['de_tipoprocesso']

    # Bruno - 10/01/2020: reescrita dos métodos 'get_queryset' e 'get_context_data' para realizar filtragem por GET
    def get_queryset(self):
        filter_val = self.request.GET.get('filter', '')
        new_context = self.model.objects.filter(de_tipoprocesso__icontains=filter_val)
        return new_context

    def get_context_data(self, **kwargs):
        context = super(TipoProcessoList, self).get_context_data(**kwargs)
        context['filter'] = self.request.GET.get('filter', '')
        return context


class TipoProcessoCreate(BSModalCreateView):
    form_class = TipoProcessoForm
    template_name = 'tipo_processo/create_tipo_processo.html'
    model = TipoProcesso
    success_url = reverse_lazy('list_tipo_processo')
    success_message = 'Tipo de Processo cadastrado com sucesso!'


class TipoProcessoUpdate(BSModalUpdateView):
    form_class = TipoProcessoForm
    template_name = 'tipo_processo/update_tipo_processo.html'
    model = TipoProcesso
    success_url = reverse_lazy('list_tipo_processo')
    success_message = 'Tipo de Processo editado com sucesso!'


class TipoProcessoDelete(BSModalDeleteView):
    template_name = 'tipo_processo/confirm_delete_tipo_processo.html'
    model = TipoProcesso
    success_url = reverse_lazy('list_tipo_processo')
    success_message = 'Tipo de Processo deletado com sucesso!'

    # Bruno - 10/01/2020: reescrita do método 'post' para realizar a validação de erro de integridade relacional.
    def post(self, request, *args, **kwargs):
        try:
            self.get_object().delete()
            messages.add_message(request, messages.SUCCESS, self.success_message)
            return redirect(self.success_url)
        except ProtectedError:
            messages.add_message(request, messages.ERROR, 'Erro: registro já está sendo utilizado!')
            return redirect(reverse_lazy('list_tipo_processo'))


"""
@Author: Bruno Morete Felix
@Date: 09/02/2020

Criação das Views referentes ao CRUD da classe 'Processo'.
"""


class ProcessoList(ListView):
    paginate_by = 5
    template_name = 'processo/list_processo.html'
    model = Processo
    ordering = ['-dt_processo']

    # Bruno - 10/01/2020: reescrita dos métodos 'get_queryset' e 'get_context_data' para realizar filtragem por GET
    def get_queryset(self):
        filter_val = self.request.GET.get('filter', '')

        if filter_val == '':
            filter_val = 0
            new_context = self.model.objects.filter(nu_processo__gte=filter_val)
        else:
            filter_val = int(filter_val)
            new_context = self.model.objects.filter(nu_processo__exact=filter_val)

        return new_context

    def get_context_data(self, **kwargs):
        context = super(ProcessoList, self).get_context_data(**kwargs)
        context['filter'] = self.request.GET.get('filter', '')
        return context


class ProcessoCreate(BSModalCreateView):
    form_class = ProcessoForm
    template_name = 'processo/create_processo.html'
    model = Processo
    success_message = 'Processo criado com sucesso!'
    success_url = reverse_lazy('list_processo')

    # Bruno - 10/01/2020: reescrita do método 'post' para tratar o arquivo PDF.
    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            # No caso do fomulário estiver correto, há a codificação dos dados do arquivo para base64
            processo = form.save(commit=False)
            processo.ar_processo = base64.b64encode(processo.ar_processo)
            processo.save()

            messages.add_message(request, messages.SUCCESS, self.success_message)

            return redirect(self.success_url)
        else:
            return render(request, self.template_name, {'form': form})


class ProcessoUpdate(BSModalUpdateView):
    form_class = ProcessoForm
    template_name = 'processo/update_processo.html'
    model = Processo
    success_url = reverse_lazy('list_processo')
    success_message = 'Processo editado com sucesso!'

    # Bruno - 10/01/2020: reescrita do método 'get' e 'post'.

    # GET: tratar a recuperação dos dados no form.
    def get(self, request, *args, **kwargs):
        processo = get_object_or_404(Processo, pk=kwargs.get('pk'))

        # Preenchimento dos valores baseados no objeto obtido
        form = self.form_class(instance=processo)

        # Tratativa do valor inicial do campo 'date' para que o HTML5 o renderize (Ele só aceita no formato YYYY-MM-DD)
        form.initial['dt_processo'] = form.initial['dt_processo'].strftime("%Y-%m-%d")

        return render(request, self.template_name, {'form': form})

    # POST: tratar o arquivo PDF
    def post(self, request, *args, **kwargs):
        processo = get_object_or_404(Processo, pk=kwargs.get('pk'))

        form = self.form_class(request.POST, request.FILES, instance=processo)
        if form.is_valid():
            # Validação para verificar se o arquivo que está sendo passado no momento é o mesmo:
            # Caso seja, ignora o upload.
            if form.initial['ar_processo'] == processo.ar_processo:
                processo_form = form.save(commit=False, ignore=True)
            # Caso contrário, há a codificação dos dados do arquivo para base64
            else:
                processo_form = form.save(commit=False)
                processo_form.ar_processo = base64.b64encode(processo.ar_processo)

            processo_form.save()

            messages.add_message(request, messages.SUCCESS, self.success_message)

            return redirect(self.success_url)

        else:
            return render(request, self.template_name, {'form': form})


class ProcessoDelete(BSModalDeleteView):
    template_name = 'processo/confirm_delete_processo.html'
    model = Processo
    success_url = reverse_lazy('list_processo')
    success_message = 'Processo deletado com sucesso!'

    # Bruno - 10/01/2020: reescrita do método 'post' para realizar a validação de erro de integridade relacional.
    def post(self, request, *args, **kwargs):
        try:
            self.get_object().delete()
            messages.add_message(request, messages.SUCCESS, self.success_message)
            return redirect(self.success_url)
        except ProtectedError:
            messages.add_message(request, messages.ERROR, 'Erro: registro já está sendo utilizado!')
            return redirect(reverse_lazy('list_processo'))


"""
@Author: Bruno Morete Felix
@Date: 11/02/2020

Criação da Function Based View referente ao download do arquivo do Processo.
"""


def download_blob(request, pk):
    processo = get_object_or_404(Processo, pk=pk)

    # Recuperação da string em base64 referente ao arquivo
    b64 = processo.ar_processo.decode('utf-8')

    # Processo de decodificação da string em base64
    arquivo = base64.b64decode(b64, validate=True)

    # Processo de retorno de uma resposta para o download do arquivo
    response = HttpResponse(arquivo, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=%s.pdf' % processo.__str__()

    return response
