from bootstrap_modal_forms.forms import BSModalForm
from django import forms
from django.forms import DateInput

from .models import Forma, TipoProcesso, Materia, Processo

"""
@Author: Bruno Morete Felix
@Date: 08/02/2020

Criação do Form referente à classe 'Forma'.
"""


class FormaForm(BSModalForm):
    class Meta:
        model = Forma
        fields = ['de_forma', 'sg_forma']
        labels = {
            'de_forma': 'Forma',
            'sg_forma': 'Sigla',
        }


"""
@Author: Bruno Morete Felix
@Date: 08/02/2020

Criação do Form referente à classe 'TipoProcesso'.
"""


class TipoProcessoForm(BSModalForm):
    class Meta:
        model = TipoProcesso
        fields = ['de_tipoprocesso', 'sg_tipoprocesso']
        labels = {
            'de_tipoprocesso': 'Tipo de Processo',
            'sg_tipoprocesso': 'Sigla',
        }


"""
@Author: Bruno Morete Felix
@Date: 08/02/2020

Criação do Form referente à classe 'Materia'.
"""


class MateriaForm(BSModalForm):
    class Meta:
        model = Materia
        fields = ['de_tipomateria', 'sg_tipomateria']
        labels = {
            'de_tipomateria': 'Matéria',
            'sg_tipomateria': 'Sigla',
        }


"""
@Author: Bruno Morete Felix
@Date: 08/02/2020

Criação do Form referente à classe 'Processo'.
"""


class ProcessoForm(BSModalForm):
    ar_processo = forms.FileField(required=True)
    ar_processo.label = 'Arquivo do Processo'
    ar_processo.widget.attrs.update({'onchange': 'ValidateFile(this)'})

    # Bruno - 10/01/2019: Reescrita do método 'save' para "limpar" os bytes do arquivo antes de transformá-lo na base64
    def save(self, commit=False, ignore=False):
        if not ignore:
            if self.cleaned_data.get('ar_processo') is not None:
                data = self.cleaned_data['ar_processo'].file.read()
                self.instance.ar_processo = data
        return self.instance

    class Meta:
        model = Processo

        fields = ['dt_processo', 'nu_processo', 'vl_processo', 'ar_processo', 'cod_forma', 'cod_materia',
                  'cod_tipoprocesso']
        labels = {
            'dt_processo': 'Data do Processo',
            'nu_processo': 'Número do Processo',
            'vl_processo': 'Valor do Processo',
            'ar_processo': 'Arquivo do Processo',
            'cod_forma': 'Forma',
            'cod_materia': 'Matéria',
            'cod_tipoprocesso': 'Tipo do Processo',
        }
        widgets = {
            'dt_processo': DateInput(attrs={'type': 'date'}),
        }
