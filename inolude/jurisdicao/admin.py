from django.contrib import admin
from jurisdicao.models import Forma, Materia, Processo, TipoProcesso

admin.site.register(Forma)
admin.site.register(Materia)
admin.site.register(Processo)
admin.site.register(TipoProcesso)
