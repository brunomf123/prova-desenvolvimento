from django.db import models

"""
@Author: Bruno Morete Felix
@Date: 08/02/2020

Criação das Classes baseadas no Banco de Dados recebido via e-mail, utilizando o comando inspectdb.

Adaptações feitas:
    -> Processo: 'vl_processo' mudado de FloatField para DecimalField 
    -> Processo: 'ar_processo' mudado de CharField para BinaryField
"""


class Forma(models.Model):
    cod_forma = models.AutoField(db_column='cod_Forma', primary_key=True)
    de_forma = models.CharField(db_column='de_Forma', max_length=45)
    sg_forma = models.CharField(db_column='sg_Forma', max_length=45)

    def __str__(self):
        return self.de_forma

    class Meta:
        db_table = 'tbforma'


class Materia(models.Model):
    cod_tipomateria = models.AutoField(db_column='cod_TipoMateria', primary_key=True)
    de_tipomateria = models.CharField(db_column='de_TipoMateria', max_length=45)
    sg_tipomateria = models.CharField(db_column='sg_TipoMateria', max_length=45, blank=True, null=True)

    def __str__(self):
        return self.de_tipomateria

    class Meta:
        db_table = 'tbmateria'


class TipoProcesso(models.Model):
    cod_tipoprocesso = models.AutoField(db_column='cod_TipoProcesso', primary_key=True)
    de_tipoprocesso = models.CharField(db_column='de_TIpoProcesso', max_length=45)
    sg_tipoprocesso = models.CharField(db_column='sg_TipoProcesso', max_length=45, blank=True, null=True)

    def __str__(self):
        return self.de_tipoprocesso

    class Meta:
        db_table = 'tbtipoprocesso'


class Processo(models.Model):
    cod_processo = models.AutoField(db_column='cod_Processo', primary_key=True)
    nu_processo = models.BigIntegerField(db_column='nu_Processo')
    dt_processo = models.DateField(db_column='dt_Processo')
    ar_processo = models.BinaryField(db_column='ar_Processo', editable=True)
    vl_processo = models.DecimalField(db_column='vl_Processo', blank=True, null=True, decimal_places=2, max_digits=21)
    cod_tipoprocesso = models.ForeignKey('TipoProcesso', on_delete=models.PROTECT, db_column='cod_TipoProcesso')
    cod_materia = models.ForeignKey(Materia, on_delete=models.PROTECT, db_column='cod_Materia')
    cod_forma = models.ForeignKey(Forma, on_delete=models.PROTECT, db_column='cod_Forma')

    def __str__(self):
        return str(self.cod_processo) + "-" + str(self.nu_processo)

    class Meta:
        db_table = 'tbprocesso'
